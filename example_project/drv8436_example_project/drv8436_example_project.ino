#define DIR 3 // PA2
#define STEP 4 // PA3
#define M1 0 // PA6

void setup() {
  pinMode(DIR, OUTPUT);
  pinMode(STEP, OUTPUT);
  pinMode(M1, OUTPUT);

  digitalWrite(DIR, HIGH);
  digitalWrite(M1, HIGH);
}

int delayus = 10;
int changetimer = 0;

void loop() {
  digitalWrite(STEP, HIGH);
  delayMicroseconds(delayus);
  digitalWrite(STEP, LOW);
  delayMicroseconds(delayus);
  if (changetimer == 100) {
    delayus++;
    if (delayus == 100) {
      delayus = 10;
    }
    changetimer = 0;
  }
  changetimer++;
}
