EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L drv8436_breakout_symbol:drv8436_breakout U1
U 1 1 6183216D
P 4050 3000
F 0 "U1" H 4050 3681 50  0000 C CNN
F 1 "drv8436_breakout" H 4050 3590 50  0000 C CNN
F 2 "drv8436_breakout_footprint:drv8436_breakout_footprint" H 4050 3000 50  0001 C CNN
F 3 "" H 4050 3000 50  0001 C CNN
	1    4050 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 1700 4050 1700
Wire Wire Line
	4050 1700 4050 2050
Wire Wire Line
	3650 1600 4050 1600
Wire Wire Line
	4050 1600 4050 1700
Connection ~ 4050 1700
$Comp
L power:GND #PWR01
U 1 1 61836CEA
P 2950 2200
F 0 "#PWR01" H 2950 1950 50  0001 C CNN
F 1 "GND" H 2955 2027 50  0000 C CNN
F 2 "" H 2950 2200 50  0001 C CNN
F 3 "" H 2950 2200 50  0001 C CNN
	1    2950 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 2200 2950 2050
Wire Wire Line
	2950 1600 3150 1600
Wire Wire Line
	3150 1700 2950 1700
Connection ~ 2950 1700
Wire Wire Line
	2950 1700 2950 1600
$Comp
L Device:C C1
U 1 1 61837773
P 3550 2050
F 0 "C1" V 3298 2050 50  0000 C CNN
F 1 "C" V 3389 2050 50  0000 C CNN
F 2 "fab:fab_C_1206" H 3588 1900 50  0001 C CNN
F 3 "~" H 3550 2050 50  0001 C CNN
	1    3550 2050
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 2050 2950 2050
Connection ~ 2950 2050
Wire Wire Line
	2950 2050 2950 1700
Wire Wire Line
	3700 2050 4050 2050
Connection ~ 4050 2050
Wire Wire Line
	4050 2050 4050 2500
Wire Wire Line
	3200 3250 3200 3300
Wire Wire Line
	3200 3300 3350 3300
Wire Wire Line
	3350 3300 3350 3050
Wire Wire Line
	3350 3050 3650 3050
Wire Wire Line
	3650 3150 3450 3150
Wire Wire Line
	3450 3150 3450 3400
Wire Wire Line
	3450 3400 3100 3400
Wire Wire Line
	3100 3400 3100 3250
Wire Wire Line
	3200 2750 3200 2700
Wire Wire Line
	3200 2700 3350 2700
Wire Wire Line
	3350 2700 3350 2950
Wire Wire Line
	3350 2950 3650 2950
Wire Wire Line
	3650 2850 3450 2850
Wire Wire Line
	3450 2850 3450 2600
Wire Wire Line
	3450 2600 3100 2600
Wire Wire Line
	3100 2600 3100 2750
$Comp
L power:GND #PWR02
U 1 1 6183CB15
P 3950 4200
F 0 "#PWR02" H 3950 3950 50  0001 C CNN
F 1 "GND" H 3955 4027 50  0000 C CNN
F 2 "" H 3950 4200 50  0001 C CNN
F 3 "" H 3950 4200 50  0001 C CNN
	1    3950 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 6183F0DC
P 4150 3800
F 0 "R1" H 4220 3846 50  0000 L CNN
F 1 "R" H 4220 3755 50  0000 L CNN
F 2 "fab:fab_R_1206" V 4080 3800 50  0001 C CNN
F 3 "~" H 4150 3800 50  0001 C CNN
	1    4150 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 3500 3950 4100
Wire Wire Line
	4150 3950 4150 4100
Wire Wire Line
	4150 4100 3950 4100
Connection ~ 3950 4100
Wire Wire Line
	3950 4100 3950 4200
Wire Wire Line
	4150 3500 4150 3600
$Comp
L power:VCC #PWR03
U 1 1 61841E22
P 4750 3400
F 0 "#PWR03" H 4750 3250 50  0001 C CNN
F 1 "VCC" H 4765 3573 50  0000 C CNN
F 2 "" H 4750 3400 50  0001 C CNN
F 3 "" H 4750 3400 50  0001 C CNN
	1    4750 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 61842FB1
P 4450 3600
F 0 "R2" V 4243 3600 50  0000 C CNN
F 1 "R" V 4334 3600 50  0000 C CNN
F 2 "fab:fab_R_1206" V 4380 3600 50  0001 C CNN
F 3 "~" H 4450 3600 50  0001 C CNN
	1    4450 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 3600 4150 3600
Connection ~ 4150 3600
Wire Wire Line
	4150 3600 4150 3650
Wire Wire Line
	4600 3600 4750 3600
Wire Wire Line
	4750 3600 4750 3400
$Comp
L fab:Conn_PinHeader_2x02_P2.54mm_Vertical_SMD J1
U 1 1 6184468E
P 3200 2950
F 0 "J1" V 3204 3030 50  0000 L CNN
F 1 "Conn_PinHeader_2x02_P2.54mm_Vertical_SMD" V 3250 1000 50  0000 L CNN
F 2 "fab:fab_CONN_2x02_SMD" H 3200 2950 50  0001 C CNN
F 3 "https://cdn.amphenol-icc.com/media/wysiwyg/files/drawing/95278.pdf" H 3200 2950 50  0001 C CNN
	1    3200 2950
	0    1    1    0   
$EndComp
$Comp
L fab:Conn_PinHeader_2x02_P2.54mm_Vertical_SMD J2
U 1 1 618468C0
P 3350 1600
F 0 "J2" H 3400 1817 50  0000 C CNN
F 1 "Conn_PinHeader_2x02_P2.54mm_Vertical_SMD" H 3400 1726 50  0000 C CNN
F 2 "fab:fab_CONN_2x02_SMD" H 3350 1600 50  0001 C CNN
F 3 "https://cdn.amphenol-icc.com/media/wysiwyg/files/drawing/95278.pdf" H 3350 1600 50  0001 C CNN
	1    3350 1600
	1    0    0    -1  
$EndComp
$Comp
L fab:Regulator_Linear_LM3480-5.0V-100mA U2
U 1 1 61847AFB
P 5100 1600
F 0 "U2" H 5100 1842 50  0000 C CNN
F 1 "Regulator_Linear_LM3480-5.0V-100mA" H 5100 1751 50  0000 C CNN
F 2 "fab:fab_SMD_SOT23" H 5100 1825 50  0001 C CIN
F 3 "https://www.ti.com/lit/ds/symlink/lm3480.pdf" H 5100 1600 50  0001 C CNN
	1    5100 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 618486DD
P 4650 1900
F 0 "C2" H 4765 1946 50  0000 L CNN
F 1 "C" H 4765 1855 50  0000 L CNN
F 2 "fab:fab_C_1206" H 4688 1750 50  0001 C CNN
F 3 "~" H 4650 1900 50  0001 C CNN
	1    4650 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 1600 4650 1600
Connection ~ 4050 1600
Wire Wire Line
	4650 1750 4650 1600
Connection ~ 4650 1600
Wire Wire Line
	4650 1600 4050 1600
$Comp
L power:GND #PWR04
U 1 1 61849C9D
P 5100 2250
F 0 "#PWR04" H 5100 2000 50  0001 C CNN
F 1 "GND" H 5105 2077 50  0000 C CNN
F 2 "" H 5100 2250 50  0001 C CNN
F 3 "" H 5100 2250 50  0001 C CNN
	1    5100 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 2250 5100 2150
Wire Wire Line
	4650 2050 4650 2150
Wire Wire Line
	4650 2150 5100 2150
Connection ~ 5100 2150
Wire Wire Line
	5100 2150 5100 1900
$Comp
L Device:C C3
U 1 1 6184B2D3
P 5650 1900
F 0 "C3" H 5765 1946 50  0000 L CNN
F 1 "C" H 5765 1855 50  0000 L CNN
F 2 "fab:fab_C_1206" H 5688 1750 50  0001 C CNN
F 3 "~" H 5650 1900 50  0001 C CNN
	1    5650 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 1750 5650 1600
Wire Wire Line
	5650 1600 5400 1600
Wire Wire Line
	5100 2150 5650 2150
Wire Wire Line
	5650 2150 5650 2050
$Comp
L power:VCC #PWR05
U 1 1 6184D0F9
P 5650 1350
F 0 "#PWR05" H 5650 1200 50  0001 C CNN
F 1 "VCC" H 5665 1523 50  0000 C CNN
F 2 "" H 5650 1350 50  0001 C CNN
F 3 "" H 5650 1350 50  0001 C CNN
	1    5650 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 1350 5650 1600
Connection ~ 5650 1600
$Comp
L fab:Microcontroller_ATtiny412_SSFR U3
U 1 1 6184EE22
P 6900 3050
F 0 "U3" H 6370 3096 50  0000 R CNN
F 1 "Microcontroller_ATtiny412_SSFR" H 6370 3005 50  0000 R CNN
F 2 "fab:fab_IC_SOIC8" H 6900 3050 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/40001911A.pdf" H 6900 3050 50  0001 C CNN
	1    6900 3050
	-1   0    0    -1  
$EndComp
$Comp
L power:VCC #PWR06
U 1 1 6184FBBF
P 6900 2100
F 0 "#PWR06" H 6900 1950 50  0001 C CNN
F 1 "VCC" H 6915 2273 50  0000 C CNN
F 2 "" H 6900 2100 50  0001 C CNN
F 3 "" H 6900 2100 50  0001 C CNN
	1    6900 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 2100 6900 2350
$Comp
L power:GND #PWR07
U 1 1 61850E04
P 6900 4150
F 0 "#PWR07" H 6900 3900 50  0001 C CNN
F 1 "GND" H 6905 3977 50  0000 C CNN
F 2 "" H 6900 4150 50  0001 C CNN
F 3 "" H 6900 4150 50  0001 C CNN
	1    6900 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 4150 6900 3750
$Comp
L fab:Conn_PinHeader_1x02_P2.54mm_Horizontal_SMD J3
U 1 1 618524AB
P 5400 2650
F 0 "J3" H 5406 2576 50  0000 R CNN
F 1 "Conn_PinHeader_1x02_P2.54mm_Horizontal_SMD" H 6250 2750 50  0000 R CNN
F 2 "fab:fab_CONN_1x02_SMD" H 5400 2650 50  0001 C CNN
F 3 "~" H 5400 2650 50  0001 C CNN
	1    5400 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2650 5600 2650
$Comp
L Device:R R3
U 1 1 61860466
P 5850 2650
F 0 "R3" V 5643 2650 50  0000 C CNN
F 1 "R" V 5734 2650 50  0000 C CNN
F 2 "fab:fab_R_1206" V 5780 2650 50  0001 C CNN
F 3 "~" H 5850 2650 50  0001 C CNN
	1    5850 2650
	0    1    1    0   
$EndComp
Wire Wire Line
	6000 2650 6100 2650
Wire Wire Line
	6100 2650 6100 2750
Wire Wire Line
	6100 2750 6300 2750
Wire Wire Line
	5600 2750 6100 2750
Connection ~ 6100 2750
Wire Wire Line
	6300 2850 4450 2850
Wire Wire Line
	4450 2950 6300 2950
Wire Wire Line
	4450 3050 6300 3050
Wire Wire Line
	6300 3250 5850 3250
Wire Wire Line
	5850 3250 5850 3150
Wire Wire Line
	5850 3150 4450 3150
$EndSCHEMATC
