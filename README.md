# DRV8436 breakout board
A tiny castellated carrier for the QFN version of TI's [DRV8436](https://www.ti.com/lit/ds/symlink/drv8436.pdf) stepper driver.
![overview](img/overview.jpg)

As used in a circuit:
![example](img/example.jpg)

Fabrication:
![fabrication](img/fabrication.jpg)

## v0.2 ("baa")
The second spin of the board breaks out the `nSleep` pin to a dedicated castellated pad, so the user can freely use `M0` and `M1` to select up to 1/256 microstepping. The assembled devices also use TI's [DRV8434](https://www.ti.com/lit/ds/symlink/drv8434.pdf) instead of the -6, a pin-compatible part which substantially increases the current handling capability of the board to 1.8 A RMS. External dimensions are unchanged, beyond a minor pad spacing change for the bottom row: `GND`, `nSLEEP`, and `VREF` are now 1.5 mm apart to match the side castellations.    

![pcb_v02](img/pcb_v02.png)

The board includes the recommended bypass capacitors from the datasheet, along with a 10k pullup resistor for `nFault`. Less typically useful pins (such as `nFault`) aren't broken out. 

![schematic_v02](img/schematic_v02.png)

To use the board, it's worth taking the time to first read the chip datasheet linked above; both the DRV8436 and DRV8434 use the same step/direction interface and M0/M1 microstepping configuration. Once you've done that:

- connect `A1`, `A2`, `B1`, and `B2` to the stepper motor coils. Make sure matching letters connect to either side of the same coil; it's worth checking with a multimeter to be sure this is correct. The steppers I've used typically are connected red/blue and green/black. 
- connect `VREF` to a voltage source to provide the current limit for the motor. The simple solution (as shown in my sample circuit) is a resistor divider; if you want to adjust this on the fly, you can connect it to a PWM pin through a lowpass filter, or to a potentiometer for manual adjustment. 
- connect `VCC` to a suitable power supply for your motor. While the chip is rated up to 48 VDC, I used 16 volt capacitors in the design so keep VCC to 12 VDC or less.
- connect `STEP` and `DIR` to a microcontroller's GPIO lines. 
- optionally connect `M0` and `M1` to a microcontroller's GPIO lines, or `GND`, or a 3.3 - 5 VDC rail, or leave them floating, depending on how you want microstepping to be configured (see datasheet).
- connect `nSLEEP` to a 3.3 - 5 VDC rail, such as a microcontroller power supply. You could also connect this to a GPIO line if you'd like to be able to turn off the stepper driver and reduce current demand on your circuit when the motor isn't being used.
- connect `GND` to the circuit ground net, which should be shared with both the microcontroller power supply and the stepper power supply. 

The example project in /example_project uses the v0.1 PCB, but beyond the lack of `nSLEEP` pin everything else is quite similar. If you use KiCad, a library symbol and footprint for v0.2 are located in /kicad_library_v02. 

## v0.1 ("moo")
The first version works, but has a dumb issue. You see, the DRV8436 has an internal voltage regulator which provides a logic-level power source so you only have to send it motor voltage. In an effort to minimize pin count, I decided to use that output to hold the `nSleep` pin high to keep the chip awake. However, the device checks `nSleep` _before_ turning on the regulator, so as-is it doesn't wake up. For this version, the fix I included simply bridges the `nSleep` pin to `M1`, which I do break out as a castellation. `M1` and `M0` are used to configure microstepping; with this modification, the user must hold `M1` high for the device to function, which limits them to 1/4, 1/8, or 1/16 microstepping only. Sorry friends. 

![pcb](img/pcb.png)

The board measures 9.5 mm tall and 7.5 mm wide. The four pads on either side are vertically centered and separated by 1.5 mm. The VCC pin is horizontally centered, while the GND and VREF pins are at +/- 1.75 mm. The design includes all of the recommended bypassing capacitors with the exception of a bulk tank cap, so it's a good idea to include a 33 or 47 uF capacitor between VCC and GND. You can supply the board with up to 12 VDC; some of the capacitors are 16 VDC units, so you shouldn't go much higher than that. Here is a sample schematic:

![schematic](img/schematic.png)

J1 connects to the stepper; J2 gets 12 VDC; J3 programs the ATtiny412; U2 is a 5 VDC regulator; C1 is a 33 uF ceramic cap; R1 and R2 are in the 5-20k range and used to set the current limit for the stepper. Again, note that M1 is held high to deal with the issue listed above. 

To use the board in a project, you can use the KiCad schematic symbol and footprint file in the `example_project` directory, or you can make your own based on the image above. As always, it's worth reading through the [chip's datasheet](https://www.ti.com/lit/ds/symlink/drv8436.pdf) to understand how it works. 